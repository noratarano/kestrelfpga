Kestrel FPGA README
===================

src
---
Verilog source files

src/coe
-------
COE files for initializing memory elements

src/arch
--------
Unused source files

lib
---
Various helper scripts

projects
--------
Xilinx Design Tools projects

projects/main
-------------
Final KestrelFPGA project
_(All other projects will have a different name)_
