`timescale 1ns / 1ps

/**
* Program Counter Stack
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly (p.82).
* Comments include the corresponding nomenclature of the KASM manual.
*
*/

module pc_stack (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire			I_POP_PC,
	input  wire			I_PUSH_PC,
	input  wire  [1:0]	I_PC_SEL,
	input  wire			I_BR_0,
	input  wire			I_BR_W_OR,
	input  wire			W_OR,
	input  wire			Cntr_Branch_inv,
	input  wire [15:0]	I_IMM,

	output wire [15:0]	PC_OUT
);

	parameter size = 16;

	wire [15:0] returnPC;
	wire		Cntr_Branch = ~Cntr_Branch_inv;
	wire		loopBack;
	reg  [15:0]	PC, PC_NEXT;

	assign PC_OUT = PC_NEXT;

	stack #(size) ProgramCounterStack (
		.CLK		(CLK),					// input  wire			CLK,
		.RST_L		(RST_L),				// input  wire			RST_L,
		.push		(I_PUSH_PC),			// input  wire			push,
		.pop		(I_POP_PC),				// input  wire			pop,
		.datain		(PC + 16'h0001),		// input  wire [15:0]	datain,
		.dataout	(returnPC)				// output wire [15:0]	dataout
	);

// PC Selector + Branch Logic
	always @* begin
		case (I_PC_SEL)
			2'b00: begin
				if ({I_BR_W_OR, I_BR_0, W_OR} == 3'b101 ||
					{I_BR_W_OR, I_BR_0, Cntr_Branch} == 3'b011) begin
					PC_NEXT = I_IMM;	// branch
				end else begin
					PC_NEXT = PC + 16'h0001;	// goto next instruction
				end
			end
			2'b01: PC_NEXT = I_IMM;		// always branch
			2'b10: PC_NEXT = returnPC;	// return from subroutine
			2'b11: PC_NEXT = PC;		// idle; don't change PC
		endcase
	end

	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			PC <= 16'hffff;	// so that PC_OUT is 0 at the beginning
		end else begin
			PC <= PC_NEXT;
		end
	end

endmodule: pc_stack

// Helper Stack Module
	module stack (
		input  wire			CLK,
		input  wire			RST_L,
		input  wire			push,
		input  wire			pop,
		input  wire [15:0]	datain,

		output reg  [15:0]	dataout
	);

	// signal instantiations
		parameter size = 16;

		reg [15:0]	memory [size-1:0];
		reg  [3:0]	head;

	// pointers logic
		always @ (posedge CLK, negedge RST_L) begin
			if (~RST_L) begin
				head <= 4'h0;
			end else begin
				if (pop) begin
					head <= head - 4'h1;
				end else if (push) begin
					head <= head + 4'h1;
				end
			end
		end

	// output register logic
		always @ (posedge CLK, negedge RST_L) begin
			if (~RST_L) begin
				dataout <= 16'h0000;
			end else if (pop) begin
				dataout <= memory[head-4'h1];
			end
		end

	// input logic
		always @ (posedge CLK, negedge RST_L) begin
			if (~RST_L) begin
				memory[head] <= 16'h0000;
			end else if (push) begin
				memory[head] <= datain;
			end
		end

	endmodule
