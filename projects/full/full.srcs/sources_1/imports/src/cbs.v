`timescale 1ns / 1ps

/**
* Controller's Bit Shifter (and WOR register)
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly. (p.82,84)
* Comments include the corresponding nomenclature of the KASM manual.
*
* Only implements the serial load of the cbs, not the parallel load.
*
*/

module cbs (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire	 		K_W_OR_IN,
	input  wire			I_CBS_SLEFT,
	input  wire			I_CBS_LOAD,

	output wire			W_OR,
	output reg	 [7:0]	BS_OUT		// cbs
);

	assign W_OR = K_W_OR_IN;

	always @(posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			BS_OUT <= 8'h00;
		end else if (I_CBS_SLEFT | I_CBS_LOAD) begin
			// TODO: may want to not enable this if I_CBS_LOAD is asserted
			BS_OUT <= {BS_OUT[6:0], W_OR};
		end else begin
			BS_OUT <= BS_OUT;
		end
	end

endmodule: cbs