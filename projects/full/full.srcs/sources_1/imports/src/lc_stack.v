`timescale 1ns / 1ps

/**
* Loop Counter Stack
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
* Follows the nomenclature of the Thesis directly. (p.82)
* Comments include the corresponding nomenclature of the KASM manual.
*
*/

module lc_stack (
	input  wire			CLK,
	input  wire			RST_L,
	input  wire  [7:0]	SCR_OUT,		// scratch
	input  wire			I_CNT_LOAD_0,	// imm_sel[0]
	input  wire			I_CNT_LOAD_1,	// imm_sel[1]
	input  wire			I_PUSH_CNT,		// cnt_ps
	input  wire			I_DEC_CNT,		// cnt_d
	input  wire			D_Pop_Cnt,		// D_pop
	input  wire [15:0]	I_IMM,			// c_imm

	output wire			Cntr_Branch_inv	// AKA loopdone, underflow
);

	parameter	size = 16;

	reg [15:0]	memory [size-1:0];
	reg  [3:0]	head;

	assign Cntr_Branch_inv = (memory[head] == 16'h0000);

// pointer logic
	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			head <= 4'h0;
		end else begin
			if (I_PUSH_CNT) begin
				head <= head + 4'h1;	// push
			end else begin
				if (I_DEC_CNT | D_Pop_Cnt) begin
					if (Cntr_Branch_inv | D_Pop_Cnt) begin
						head <= head - 4'h1;	// pop
					end
				end
			end
		end
	end

// input logic
	always @ (posedge CLK, negedge RST_L) begin
		if (~RST_L) begin
			memory[4'h0] <= 16'hffff;
			memory[4'hf] <= 16'hffff;
		end else begin
			if (I_PUSH_CNT) begin
				memory[head+4'h1] <= I_IMM;	// push
			end
			if (I_DEC_CNT) begin
		  		memory[head] <= memory[head] - 16'h0001;	// decrement
			end else if (I_CNT_LOAD_0) begin
				memory[head] <= {memory[head][15:8], SCR_OUT};	// scrtolo
			end else if (I_CNT_LOAD_1) begin
				memory[head] <= {SCR_OUT, memory[head][7:0]};	// scrtohi
			end
		end
	end

endmodule: lc_stack