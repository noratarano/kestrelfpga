`timescale 1ns / 1ps
/* top.v
	Nora Tarano
	July 2013
																*/


module top (
	input  wire			CLK,
	input  wire			RST,
	input  wire			READ_L,

	output reg  [7:0]	led
);

	wire [51:0]	instruction;
	wire		qtoarr;
	wire  [7:0]	fromLeft, fromRight;
	wire  [7:0] offLeft, offRight;
	wire		offWOR;
	wire  [8:0] outLeft, outRight;
	wire  [8:0]	read_out_L, read_out_R;

	assign outLeft = {offWOR, offLeft};
	assign outRight = {offWOR, offRight};

	PES DataPath (
		.CLK			(CLK),			// input  wire			CLK
		.RST			(RST),			// input  wire			RSTo
		.qtoarr			(qtoarr),		// input  wire			qtoarr
		.instructionIn	(instruction),	// input  wire [51:0]	instructionIn
		.fromLeft		(fromLeft),		// input  wire  [7:0]	fromLeft,		// ssr data from offchip
		.fromRight		(fromRight),	// input  wire  [7:0]	fromRight,		// ssr data from offchip
		.offLeft		(offLeft),		// output wire  [7:0]	offLeft,		// ssr data going left offchip
		.offRight		(offRight),		// output wire  [7:0]	offRight,		// ssr data going right offchip
		.offWOR			(offWOR)		// output wire			offWOR			// wor signal going (right) offchip
	);

	issuer InstructionIssuer (
		.CLK			(CLK),			// input  wire			CLK
		.RST			(RST),			// input  wire			RST
		.instruction	(instruction),	// output wire [51:0]	instruction
		.qtoarr			(qtoarr),		// output wire			qtoarr
		.fromLeft		(fromLeft),		// output wire  [7:0]	fromLeft
		.fromRight		(fromRight)		// output wire  [7:0]	fromRight
	);

	gOutput Output (
		.CLK			(CLK),			// input  wire			CLK
		.RST			(RST),			// input  wire			RST
		.dina			(outLeft),		// input  wire [8:0]	dina
		.dinb			(outRight),		// input  wire [8:0]	dinb
		.douta			(read_out_L),	// output wire [8:0]	douta
		.doutb			(read_out_R)	// output wire [8:0]	doutb
	);

	always @(posedge CLK) begin
		if (RST) begin
			led <= 8'd0;
		end
		else begin
			led <= READ_L ? read_out_L[7:0] : read_out_R[7:0];
		end
	end

endmodule
