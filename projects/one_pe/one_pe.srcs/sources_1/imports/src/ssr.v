/* ssr.v
	Shared Systolic Registers
	Andrew W. Hill
	August 2006

	One write port, two read ports.

	Data will come from either right or left.  However,
	only one input for the	register desired is needed.
	It is assumed that the PEs are designed such that
	the direction only affects the WE/OEs, not the
	whichRegs.

	EDITS:
		Date		Time	User		Description of Change
		06/13		?		ntarano		Changed style
		07/10/13	6:34pm	ntarano		Changed the implementation to use custom IP
																					*/

module ssr (
	input  wire			CLK,
	input  wire			RST,
	input  wire [4:0]	whichRegA,			// register selection
	input  wire [4:0]	whichRegB,
	input  wire [4:0]	whichRegIn,
	input  wire			fromLeft,
	input  wire [7:0]	dataInfromLeft,		// data from PE to SSR
	input  wire [7:0]	dataInfromRight,
	input  wire			weL,				// write enable
	input  wire			weR,				// write enable

	output wire [7:0]	dataOutA,			// data from SSR to PE
	output wire [7:0]	dataOutB
);

// Port A is Left, Port B is Right

// signal instantiations
	wire		wea, web, ena, enb;
	wire [4:0]	addra, addrb;

// value assignements
	assign wea = weL & fromLeft;
	assign web = weR & ~fromLeft;
	assign addra = wea ? whichRegIn : whichRegA;
	assign addrb = web ? whichRegIn : whichRegB;

// custom ip instantiations
	// NORA: this could be weird, but ENA and ENB = 1.
	ssr_bram Registers (
		.clka	(CLK),				// input clka
		.rsta	(RST),				// input rsta
		.ena	(1'b1),				// input ena
		.wea	(wea),				// input [0 : 0] wea
		.addra	(addra),			// input [4 : 0] addra
		.dina	(dataInfromLeft),	// input [7 : 0] dina
		.douta	(dataOutA),			// output [7 : 0] douta
		.clkb	(CLK),				// input clkb
		.rstb	(RST),				// input rstb
		.enb	(1'b1),				// input enb
		.web	(web),				// input [0 : 0] web
		.addrb	(addrb),			// input [4 : 0] addrb
		.dinb	(dataInfromRight),	// input [7 : 0] dinb
		.doutb	(dataOutB)			// output [7 : 0] doutb
	);

endmodule