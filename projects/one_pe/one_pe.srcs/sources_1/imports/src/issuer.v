`timescale 1ns / 1ps

module issuer (
	input  wire			CLK,
	input  wire			RST,
	output wire [51:0]	instruction,
	output wire			qtoarr,
	output wire  [7:0]	fromLeft,
	output wire  [7:0]	fromRight
);

// signal instantiations
	reg   [4:0]	pc;
	wire [63:0]	memOut;

// custom ip instantiation, 32x64bit
	inst_bram Memory (
		.clka	(CLK),		// input clka
		.rsta	(RST),		// input rsta
		.addra	(pc),		// input [4 : 0] addra
		.douta	(memOut)	// output [63 : 0] douta
	);

// logic
	assign instruction = memOut[63:12];
	assign qtoarr = memOut[8];				// burn 3 bits to make reading instructions easier	NORA: ???
	assign fromLeft = memOut[7:0];
	assign fromRight = memOut[13:6];

	always @ (posedge CLK) begin
		if (RST) begin
			pc <= 5'd0;
		end else begin
			pc <= pc + 5'd1;
		end
	end

endmodule