/* pe.v
	The PE!
	Andrew W. Hill
	October 2006

	LOCATION OF SPECIAL PURPOSE REGISTERS:
		bs			- bs.v
		eqLatch		- cmp.v
		minLatch	- cmp.v
		mdr 		- sram.v
		mhi 		- mult.v

	EDITS:
		Date		Time	User		Description of Change
		06/13		?		ntarano		Changed style
		07/10/13	6:00pm	ntarano		Changed the mem module to sram
*/

module pe (

	input  wire			CLK,
	input  wire			RST,

	input  wire			frce,			// force
	input  wire [4:0]	alufunc,
	input  wire			ci,				// carryin
	input  wire			mp,
	input  wire			lc,				// appears to be the mult/alu switch
	input  wire			fi,
	input  wire [4:0]	flagbus,
	input  wire [3:0]	bitshift,
	input  wire [1:0]	resmx,
	input  wire			sramRead,
	input  wire			sramWrite,
	input  wire [2:0]	opBsel,
	input  wire			fromRightA,
	input  wire			fromRightC,
	input  wire [7:0]	arrayimmediate,

	input  wire [7:0]	inLA,
	input  wire [7:0]	inLC,
	input  wire [7:0]	inRA,
	input  wire [7:0]	inRC,

	input  wire			worin,

	output wire			worout,
	output wire [7:0]	out,
	output wire			on
);


// wire instantiations
	wire [7:0]	opA_fromSel;
	wire [7:0]	opB_fromSel;
	wire [7:0]	opC_fromSel;
	wire [7:0]	result_fromALU;
	wire [7:0]	result_fromComp;
	wire [7:0]	result_fromMult;
	wire [7:0]	result_fromSelector;
	wire		eq_fromComp;
	wire		min_fromComp;
	wire [7:0]	address_fromGen;
	wire		carryout_fromALU;

	// from internal registers
	wire [7:0]	mdr;
	wire [7:0]	mhi;
	wire [7:0]	bs;
	wire		eqLatch;
	wire		minLatch;

	wire		fb;		// the actual flag bus
	wire		mask;
	wire		thiswor;


// value assigns

	assign out = result_fromSelector;	// to make naming easier

	assign on = mask | frce;	// PE is on if mask indicates, or if forced.

	/* NOTE: allow wor to be affected by a wor coming in
	   this may not be necessary at all, in which case
	   worin can be grounded, or completely omitted. */
	assign thiswor = mask & bs[7];
	assign worout = thiswor | worin;


// module instantiations

	opsel SelectOperands (
		.inLA		(inLA),
		.inLC		(inLC),
		.inRA		(inRA),
		.inRC		(inRC),
		.fromRightA	(fromRightA),
		.fromRightC	(fromRightC),
		.opBsel		(opBsel),
		.mdr		(mdr),
		.mhi		(mhi),
		.bs			(bs),
		.immediate	(arrayimmediate),
		.operandA	(opA_fromSel),
		.operandB	(opB_fromSel),
		.operandC	(opC_fromSel)
	);

	alu CrudeALU (
		.CLK		(CLK),
		.RST		(RST),
		.opcode		(alufunc),
		.lc			(lc),
		.operandA	(opA_fromSel),
		.operandB	(opB_fromSel),
		.bc			(ci),
		.mp			(mp),
		.result		(result_fromALU),
		.co			(carryout_fromALU)
	);

	mult Multiplier (
		.CLK		(CLK),
		.RST		(RST),
		.operandA	(opA_fromSel),
		.operandB	(opB_fromSel),
		.SA			(alufunc[1]),
		.SB			(alufunc[2]),
		.operandC	(opC_fromSel),
		.addmcEn	(alufunc[4]),
		.addmhiEn	(alufunc[0]),
		.on			(on),
		.result		(result_fromMult),
		.mhi		(mhi)
	);

	cmp Comparator (
		.CLK		(CLK),
		.RST		(RST),
		.inputA		(result_fromALU),
		.inputB		(opC_fromSel),
		.resmx		(resmx),
		.max		(fi),
		.sign		(flagbus[0]),
		.on			(on),
		.result 	(result_fromComp),
		.eq			(eq_fromComp),
		.min		(min_fromComp),
		.eqLatch	(eqLatch),
		.minLatch	(minLatch)
	);

	addgen AddressGenerator (
		.base		(opC_fromSel),
		.offset		(arrayimmediate),
		.result		(address_fromGen)
	);

	sram SRAM (
		.CLK		(CLK),
		.RST		(RST),
        .datain		(out),
		.address	(address_fromGen),
		.WE			(sramWrite),
		.OE			(sramRead),
		.on			(on),
		.mdr		(mdr)
	);

	result SelectResult (
		.inputA		(result_fromMult),
		.inputB		(result_fromComp),
		.sel		(alufunc[3] & ~lc),
		.result		(result_fromSelector)
	);

	bs BitShifter (
		.CLK		(CLK),
		.RST		(RST),
		.opcode		(bitshift),
		.flagin		(fb),
		.resultin	(result_fromSelector),
		.bitshifter	(bs),
		.maskflag	(mask)
	);

	// NOTE: some inputs set to 0 until i decide what to do with them
	flag FlagGenerator (
		.flagbus	(flagbus),
		.ALUco		(carryout_fromALU),
		.ALUcts		(1'b0),
		.CMPbo		(1'b0),
		.CMPco		(1'b0),
		.CMPmsb		(1'b0),
		.CMPcts		(1'b0),
		.bs			(bs),
		.eq			(eq_fromComp),
		.min		(min_fromComp),
		.eqLatch	(eqLatch),
		.minLatch	(minLatch),
		.worin		(thiswor),
		.flag		(fb)
	);

endmodule