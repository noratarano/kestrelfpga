/* mem.v
	256x8b memory
	Nora Tarano
	July 2013
																*/

module sram (
	input  wire			CLK,
	input  wire			RST,
	input  wire [7:0]	datain,
	input  wire [7:0]	address,	// address
	input  wire			WE,			// write enable
	input  wire			OE,			// allows alteration of the mdr
	input  wire			on,

	output reg  [7:0]	mdr
);

// signal instantiations
	wire [7:0]	dataout;

// custom ip instantiation
	sram_bram Mem (
		.clka	(CLK),		// input clka
		.rsta	(RST),		// input rsta
		.ena	(on),		// input ena
		.wea	(WE),		// input [0:0] wea
		.addra	(address),	// input [7:0] addra
		.dina	(datain),	// input [7:0] dina
		.douta	(dataout)	// output [7:0] douta
	);

// MDR code
	always @ (posedge CLK) begin
		if (RST) begin
			mdr <= 8'h00;
		end else if (on) begin
			if (OE) begin
				mdr <= dataout;
			end else begin
				mdr <= mdr;
			end
		end else begin
			mdr <= mdr;
		end
	end

endmodule