/* mem.v
	256x8b memory
	Andrew W. Hill
	August 2006

	This is unlikely to be the right way to do this.
	However, XST sees this module the way I intended.

	XST reports 128 4input LUTs used for the RAM, or 168 for
	this entire module.  Consider that a Spartan4 has ~12k LUTs
	in basic versions, a 32PE array might be possible on
	a single chip.  XST also shows times in the 4-5ns range,
	which is favorable towards my 20ns goal.
		                                                   		*/

module mem (
	input  wire			CLK,
	input  wire			RST,
	input  wire [7:0]	datain,
	input  wire [7:0]	address,	// address
	input  wire			WE,			// write enable
	input  wire			OE,			// allows alteration of the mdr
	input  wire			on,
	output reg  [7:0]	mdr
);

	reg [7:0]	sram [255:0];		// NOTE: this had better get optimized!
									// will look for primitives later when i get web access

/* NOTE: this block violates a ton of coding guidelines */
	always @ (posedge CLK) begin
		if (WE & on) begin
			sram[address] <= datain;
		end
	end

// MDR code
	always @ (posedge CLK) begin
		if (RST) begin
			mdr <= 8'h00;
		end else if (on) begin
			if (OE) begin
				mdr <= sram[address];
			end else begin
				mdr <= mdr;
			end
		end else begin
			mdr <= mdr;
		end
	end

endmodule