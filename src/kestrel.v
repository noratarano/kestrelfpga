`timescale 1ns / 1ps

/**
* Kestrel SIMD Processor
*
* Author: Nora Tarano (ntarano@stanford.edu)
*
* Based on designs by Andrew Hill and Kestrel Arch.
*
* Conventions:
*	- Controller instruction fields are labeled I_NAME
* 	- PE instruction fields are labeled i_name
*	- Follows the nomenclature of the Thesis as much as possible
* 	  Comments include the corresponding nomenclature of the KASM manual
*/

module kestrel (
	input  wire			CLK,
	input  wire			RST_L,

	output reg  [7:0]	led
);

// signal declarations
	wire		I_DATA_READ;
	wire [52:0]	PEInstruction;
	wire  [7:0]	dataToArray;
	wire  [7:0]	PEtoControlLeft, PEtoControlRight;
	wire		WOR;

// module instantiations
	array #(.NPES(64)) Array (
		.CLK				(CLK),				// input  wire			CLK
		.RST_L				(~RST_L),			// input  wire			RST_L
		.instruction		(PEInstruction),	// input  wire [52:0]	instruction
		.I_DATA_READ		(I_DATA_READ),		// input  wire			I_DATA_READ
		.dataFromControl	(dataToArray),		// input  wire  [7:0]	dataInLeft
		.dataOutLeft		(PEtoControlLeft),	// output wire  [7:0]	dataOutLeft
		.dataOutRight		(PEtoControlRight),	// output wire  [7:0]	dataOutRight
		.offWOR				(WOR)				// output wire			offWOR
	);

	control Controller (
		.CLK				(CLK),				// input  wire			CLK,
		.RST_L				(~RST_L),			// input  wire			RST_L,
		.dataInLeft			(PEtoControlLeft),	// input  wire  [7:0]	dataInLeft,
		.dataInRight		(PEtoControlRight),	// input  wire  [7:0]	dataInRight,
		.inWOR				(WOR),				// input  wire			inWOR,
		.PEInstruction		(PEInstruction),	// output wire [52:0]	PEInstruction,
		.I_DATA_READ		(I_DATA_READ),		// output wire  		I_DATA_READ,
		.dataToArray		(dataToArray)		// output wire  [7:0]	dataToArray
	);

	always @(posedge CLK, posedge RST_L) begin
		if (RST_L) begin
			led <= 8'h00;
		end else begin
			led <= PEtoControlRight;
		end
	end

endmodule
